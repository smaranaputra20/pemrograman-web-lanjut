<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MahasiswaController extends Controller
{
    public function index()
    {
        //mengambil data dari tabel nama_mahasiswa
        $nama_mahasiswa = DB::table('nama_mahasiswa')->get();

        //mengambil data nama_mahasiswa ke view index2
        return view('index2', ['nama_mahasiswa' => $nama_mahasiswa]);
        
    }

    public function tambah()
    {
        return view('tambah');
    }

    // method untuk insert data ke table Mahasiswa
    public function store(Request $request)
    {
        // insert data ke table Mahasiswa
        DB::table('nama_mahasiswa')->insert([
            'nama_mahasiswa' => $request->nama,
            'nim_mahasiswa' => $request->nim,
            'kelas_mahasiswa' => $request->kelas,
            'prodi_mahasiswa' => $request->prodi,
            'fakultas_mahasiswa' => $request->fakultas
        ]);
        // alihkan halaman ke halaman Mahasiswa
        return redirect('/input');
 
    }

    // method untuk edit data Mahasiswa
    public function edit($id)
    {
        // mengambil data mahasiswa berdasarkan id yang dipilih
        $nama_mahasiswa = DB::table('nama_mahasiswa')->where('id',$id)->get();
        // passing data mahasiswa yang didapat ke view edit.blade.php
        return view('edit',['nama_mahasiswa' => $nama_mahasiswa]);
    
    }

    // update data Mahasiswa
    public function update(Request $request)
    {
        // update data Mahasiswa
        DB::table('nama_mahasiswa')->where('id',$request->id)->update([
            'nama_mahasiswa' => $request->nama,
            'nim_mahasiswa' => $request->nim,
            'kelas_mahasiswa' => $request->kelas,
            'prodi_mahasiswa' => $request->prodi,
            'fakultas_mahasiswa' => $request->fakultas
        ]);
        // alihkan halaman ke halaman Mahasiswa
        return redirect('/input');
    }
    // method untuk hapus data mahasiswa
    public function hapus($id)
    {
        // menghapus data mahasiswa berdasarkan id yang dipilih
        DB::table('nama_mahasiswa')->where('id',$id)->delete();
            
        // alihkan halaman ke halaman mahasiswa
        return redirect('/input');
    }
}
