<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;
use App\Http\Controllers\MahasiswaController;

Route::get('/', function () {
    return view('welcome');
});

// Route::view('/myprofile', 'profile1');


// Route::get('/mantap', function () {
//     return view('A6A6', ['nama'=>'Sugioni']);
// });

// Route::get('/myprofile', function (){
//     return view('profile');
// });

// Route::get('/home', function (){
//     return view('home', ['nama'=>'Smarana Putra']);
// });

Route::get('/home', [IndexController::class,'home']);
Route::get('/myprofile', [IndexController::class,'about']);

//get method
Route::get('/input', 'App\Http\Controllers\MahasiswaController@index' );
Route::get('/input/tambah', 'App\Http\Controllers\MahasiswaController@tambah' );
Route::get('/input/edit/{id}', 'App\Http\Controllers\MahasiswaController@edit' );
Route::get('/input/hapus/{id}', 'App\Http\Controllers\MahasiswaController@hapus' );
//post method
Route::post('/input/store', 'App\Http\Controllers\MahasiswaController@store' );
Route::post('/input/update', 'App\Http\Controllers\MahasiswaController@update' );


Route::get('/crudtemp', function (){
    return view('inputdata');
});
