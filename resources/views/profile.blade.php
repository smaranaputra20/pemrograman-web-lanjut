@extends('index')

@section('judul')
    My Profile
@endsection

@section('konten')

    <body class="about">

    <!-- Header Starts -->
    <header class="header" id="navbar-collapse-toggle">
        <!-- Fixed Navigation Starts -->
        <ul class="icon-menu d-none d-lg-block revealator-slideup revealator-once revealator-delay1">
            <li class="icon-box">
                <i class="fa fa-home"></i>
                <a href="/">
                    <h2>Home</h2>
                </a>
            </li>
            <li class="icon-box active">
                <i class="fa fa-user"></i>
                <a href="myprofile">
                    <h2>About</h2>
                </a>
            </li>
        </ul>
        <!-- Fixed Navigation Ends -->
        <!-- Mobile Menu Starts -->
        <nav role="navigation" class="d-block d-lg-none">
            <div id="menuToggle">
                <input type="checkbox" />
                <span></span>
                <span></span>
                <span></span>
                <ul class="list-unstyled" id="menu">
                    <li><a href="index.html"><i class="fa fa-home"></i><span>Home</span></a></li>
                    <li class="active"><a href="about.html"><i class="fa fa-user"></i><span>About</span></a></li>
                    <li><a href="portfolio.html"><i class="fa fa-folder-open"></i><span>Portfolio</span></a></li>
                    <li><a href="contact.html"><i class="fa fa-envelope-open"></i><span>Contact</span></a></li>
                    <li><a href="blog.html"><i class="fa fa-comments"></i><span>Blog</span></a></li>
                </ul>
            </div>
        </nav>
        <!-- Mobile Menu Ends -->
    </header>
    <!-- Header Ends -->
    <!-- Page Title Starts -->
    <section class="title-section text-left text-sm-center revealator-slideup revealator-once revealator-delay1">
        <h1>ABOUT <span>ME</span></h1>
        <span class="title-bg">Resume</span>
    </section>
    <!-- Page Title Ends -->
    <!-- Main Content Starts -->
    <section class="main-content revealator-slideup revealator-once revealator-delay1">
        <div class="container">       
                <!-- Personal Info Starts -->
            <style>
                .judul{
                    font-family: Georgia;  
                    font-size: 18px;
                    font-weight: 200;
                    font-stretch: semi-expanded;
                    opacity: 0.4;
                    margin-left: 7.5cm
                }
                .judul2{
                    font-family: Georgia;  
                    font-size: 20px;
                    font-weight: 200;
                    font-stretch: semi-expanded;
                    opacity: 0.4;
                    margin-left: 1.5cm
                }
            </style>
                    <div class="row">
                            <div class="col-12">
                                <h3 class="text-uppercase pb-4 pb-sm-5 mb-3 mb-sm-0 text-left text-sm-center custom-title ft-wt-600">info pribadi</h3>
                            </div>
                            <div class="col-6">
                                <ul class="about-list list-unstyled open-sans-font">
                                    <li> <span class="judul">First name :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Smarana</span> </li>
                                    <li> <span class="judul">Last name :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Putra</span> </li>
                                    <li> <span class="judul">Age :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">20 Tahun</span> </li>
                                    <li> <span class="judul">Nationality :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Indonesia</span> </li>
                                    <li> <span class="judul">Freelance :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Jomblo</span> </li>
                                </ul>
                            </div>
                            <div class="col-6">
                                <ul class="about-list list-unstyled open-sans-font">
                                    <li> <span class="judul2">Address :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Negara, Jembrana, Bali</span> </li>
                                    <li> <span class="judul2">Phone :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">082145884994</span> </li>
                                    <li> <span class="judul2">Email :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">smarana20@gmail.com</span> </li>
                                    <li> <span class="judul2">Instagram :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">smarana putra</span> </li>
                                    <li> <span class="judul2">Languages :</span> <span class="value d-block d-sm-inline-block d-lg-block d-xl-inline-block">Indonesia, Bali</span> </li>
                                </ul>
                            </div>
                            <div class="col-12 mt-3 text-sm-center">
                                <a href="myprofile" class="btn btn-download ">Download CV</a>
                            </div>
                    </div>
                
                <!-- Personal Info Ends -->          
                <hr class="separator">
                <style>
                    .content2{
                        display: flex;
                        justify-content: space-between
                    }
                    .hobby{
                        margin-left: 4.9cm
                    }
                    .character{
                        margin-right: 3.7cm
                    }

                </style>

                    <div class="col-12 row content2">
                        <h3 class="text-uppercase pb-4 pb-sm-5 mb-3 mb-sm-0 hobby text-left custom-title ft-wt-600">hobby</h3>
                        <h3 class="text-uppercase pb-4 pb-sm-5 mb-3 mb-sm-0 character text-left custom-title ft-wt-600">karakter</h3>
                    </div>
    
                <style>
                    .konten{
                        display: flex;
                        justify-content: left;
                    }
                    .konten2{
                        display: flex;
                        justify-content: right;
                        
                    }
                </style>
                        <div class="row"> 
                            <div class="col-6 col-md-3 mb-3 mb-sm-5 konten">
                                <div class="box-stats">
                                    <h3 class="poppins-font position-relative">85</h3>
                                    <p class="open-sans-font m-0 position-relative text-uppercase"> Movie <span class="d-block">Anime</span></p>
                                </div>
                            </div>
                            <div class="col-6 col-md-3 mb-3 mb-sm-5 konten">
                                <div class="box-stats">
                                    <h3 class="poppins-font position-relative">65</h3>
                                    <p class="open-sans-font m-0 position-relative text-uppercase"> Travel <span class="d-block">Survive</span></p>
                                </div>
                            </div>
                            <div class="col-6 col-md-3 mb-3 mb-sm-5 konten2">
                                <div class="box-stats">
                                    <h3 class="poppins-font position-relative">65</h3>
                                    <p class="open-sans-font m-0 position-relative text-uppercase"> Friendly <span class="d-block">Yamete</span></p>
                                </div>
                            </div>
                            
                            <div class="col-6 col-md-3 mb-3 mb-sm-5 konten2">
                                <div class="box-stats">
                                    <h3 class="poppins-font position-relative">55</h3>
                                    <p class="open-sans-font m-0 position-relative text-uppercase">Good Boy<span class="d-block">lol</span></p>
                                </div>
                            </div>
                            <div class="col-6 col-md-3 mb-3 mb-sm-5 konten">
                                <div class="box-stats">
                                    <h3 class="poppins-font position-relative">90</h3>
                                    <p class="open-sans-font m-0 position-relative text-uppercase">Games<span class="d-block">ML</span></p>
                                </div>
                            </div>
                            <div class="col-6 col-md-3 mb-3 mb-sm-5 konten">
                                <div class="box-stats">
                                    <h3 class="poppins-font position-relative">55</h3>
                                    <p class="open-sans-font m-0 position-relative text-uppercase">Sport<span class="d-block">chess</span></p>
                                </div>
                            </div>
                            <div class="col-6 col-md-3 mb-3 mb-sm-5 konten2">
                                <div class="box-stats">
                                    <h3 class="poppins-font position-relative">90</h3>
                                    <p class="open-sans-font m-0 position-relative text-uppercase">Sometime <span class="d-block">Badboy</span></p>
                                </div>
                            </div>
                            <div class="col-6 col-md-3 mb-3 mb-sm-5 konten2">
                                <div class="box-stats">
                                    <h3 class="poppins-font position-relative">90</h3>
                                    <p class="open-sans-font m-0 position-relative text-uppercase">Romantic<span class="d-block">hehehe</span></p>
                                </div>
                            </div>
                        </div>

            <hr class="separator">
 
            <div class="row">
                <div class="col-12">
                    <h3 class="text-uppercase pb-4 pb-sm-5 mb-3 mb-sm-0 text-left text-sm-center custom-title ft-wt-600">Kemampuan</h3>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p25">
                        <span>25%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">html</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p25">
                        <span>25%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">javascript</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p25">
                        <span>25%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">css</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p25">
                        <span>25%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">php</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p25">
                        <span>25%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">mc.word</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p50">
                        <span>50%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">querry</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p25">
                        <span>25%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">angular</h6>
                </div>
                <div class="col-6 col-md-3 mb-3 mb-sm-5">
                    <div class="c100 p55">
                        <span>55%</span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h6 class="text-uppercase open-sans-font text-center mt-2 mt-sm-4">react</h6>
                </div>
            </div>
         
            <hr class="separator mt-1">
  
            <div class="row">
                <div class="col-12">
                    <h3 class="text-uppercase pb-5 mb-0 text-left text-sm-center custom-title ft-wt-600">Riwayat<span> </span>Pendidikan</h3>
                </div>
                <div class="col-lg-6 m-15px-tb">
                    <div class="resume2-box">
                        <ul>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-graduation-cap"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">2007</span>
                                <h5 class="poppins-font text-uppercase">Sekolah Dasar<span class="place open-sans-font">SD Negeri 5 Dauhwaru</span></h5>
                                <p class="open-sans-font">Anak SD biasa yang menempuh pendidikan secara biasa juga</p>
                            </li>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-graduation-cap"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">2013</span>
                                <h5 class="poppins-font text-uppercase">Sekolah Menengah Pertama <span class="place open-sans-font">SMP Negeri 1 Negara</span></h5>
                                <p class="open-sans-font">Anak SMP biasa yang menempuh pendidikan secara biasa juga, deskripsi sama aja sih</p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 m-15px-tb">
                    <div class="resume2-box">
                        <ul>

                            <li>
                                <div class="icon">
                                    <i class="fa fa-graduation-cap"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">2016</span>
                                <h5 class="poppins-font text-uppercase">Sekolah Menengah Atas <span class="place open-sans-font">SMA Negeri 2 Negara</span></h5>
                                <p class="open-sans-font">Anak SMA biasa yang menempuh pendidikan secara biasa juga, deskripsi sama aja sih tinggal copas doang</p>
                            </li>
                            
                            <li>
                                <div class="icon">
                                    <i class="fa fa-graduation-cap"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">2019</span>
                                <h5 class="poppins-font text-uppercase">Pendidikan Teknik Informatika <span class="place open-sans-font">Universitas Pendidikan Ganesha</span></h5>
                                <p class="open-sans-font">Salah satu spesies manusia berkedok mahasiswa yang mendapatkan gelar
                                    mahasiswa kupu - kupu pada tahun pertama, #kalau ini copas dari deskripsi halaman home :v</p>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
            <hr class="separator mt-1">
            <div class="row">
                <div class="col-12">
                    <h3 class="text-uppercase pb-5 mb-0 text-left text-sm-center custom-title ft-wt-600"> Prestasi <span>&</span> Pencapaian </h3>
                </div>
                <div class="col-lg-6 m-15px-tb">
                    <div class="resume2-box">
                        <ul>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-trophy"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">2018</span>
                                <h5 class="poppins-font text-uppercase">JUARA III<span class="place open-sans-font">Tingkat SMA/SMK Sederajat</span></h5>
                                <p class="open-sans-font">Kompetisi Renang Tingkat Kabupaten Jembrana, Bali</p>
                            </li>
                            <li>
                                <div class="icon">
                                    <i class="fa fa-trophy"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">2019</span>
                                <h5 class="poppins-font text-uppercase">JUARA III <span class="place open-sans-font">Jurusan Teknik Informatika</span></h5>
                                <p class="open-sans-font">Kompetisi Teknik Informatika Basket CUP 2019</p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6 m-15px-tb">
                    <div class="resume2-box">
                        <ul>

                            <li>
                                <div class="icon">
                                    <i class="fa fa-trophy"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">2019</span>
                                <h5 class="poppins-font text-uppercase">Panitia PAT<span class="place open-sans-font">Fakultas Teknik Informatika</span></h5>
                                <p class="open-sans-font">Panitia Pagelaran Akhir Tahun Universitas Pendidikan Ganesha, Fakultas Teknik dan Kejuruan</p>
                            </li>
                            
                            <li>
                                <div class="icon">
                                    <i class="fa fa-trophy"></i>
                                </div>
                                <span class="time open-sans-font text-uppercase">2018</span>
                                <h5 class="poppins-font text-uppercase">JUARA I<span class="place open-sans-font">Vokal Group Tingkat Kabupaten</span></h5>
                                <p class="open-sans-font">Lomba Vokal Group SMA/SMK Sederajat Tingkat Kabupaten Jembrana, Bali</p>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
            
        </div>
       
    </section>

    </body>
    
@endsection

