@extends('index')

@section('judul')
    Home
@endsection

@section('konten')
    
<body class="home">
<!-- Live Style Switcher Ends - demo only -->
<!-- Header Starts -->
<header class="header" id="navbar-collapse-toggle">
    <!-- Fixed Navigation Starts -->
    <ul class="icon-menu d-none d-lg-block revealator-slideup revealator-once revealator-delay1">
        <li class="icon-box active">
            <i class="fa fa-home"></i>
            <a href="home">
                <h2>Home</h2>
            </a>
        </li>
        <li class="icon-box">
            <i class="fa fa-user"></i>
            <a href="myprofile">
                <h2>About</h2>
            </a>
        </li>
        
    </ul>
    <!-- Mobile Menu Ends -->
</header>

<section class="container-fluid main-container container-home p-0 revealator-slideup revealator-once revealator-delay1">
    <div class="color-block d-none d-lg-block"></div>
    <div class="row home-details-container align-items-center">
        <div class="col-lg-4 bg position-fixed d-none d-lg-block"></div>
        <div class="col-12 col-lg-8 offset-lg-4 home-details text-left text-sm-center text-lg-left">
            <div>
                <img src="image/smarana01.jpg" class="img-fluid main-img-mobile d-none d-sm-block d-lg-none" alt="my picture" />
                <h6 class="text-uppercase open-sans-font mb-0 d-block d-sm-none d-lg-block">hai !</h6>
                    <h1 class="text-uppercase poppins-font"><span>Saya</span> {{$nama}} </h1>
                    <p class="open-sans-font">Salah satu spesies manusia berkedok mahasiswa yang mendapatkan gelar
                                              mahasiswa kupu - kupu pada tahun pertama dan masih menempuh jenjang  
                                              Pendidikan Teknik Informatika di Universitas Pendidikan Ganesha. Makasi!</p>
                    <div ><a href="myprofile" class="btn btn-about">more about me</a></div>
            </div>
        </div>
    </div>
</section>
<!-- Main Content Ends -->

</body>
@endsection

